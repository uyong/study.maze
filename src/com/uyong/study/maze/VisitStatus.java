package com.uyong.study.maze;

/**
 * @author gonggy
 */
public class VisitStatus {
	public boolean up, down, left, right;

	/**
	 * 四个方向是否全部都访问过(全部为true)
	 * 
	 * @return
	 */
	public boolean isAllVisited() {
		return up && down && left && right;
	}

	/**
	 * 重置为未访问状态(false)
	 */
	public void reset() {
		up = down = left = right = false;
	}
}