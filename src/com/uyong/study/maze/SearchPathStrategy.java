package com.uyong.study.maze;

import java.awt.Point;

/**
 * @author gonggy12040
 */
public interface SearchPathStrategy {

	Point[] search(int[][] array);
}
