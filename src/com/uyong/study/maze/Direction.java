package com.uyong.study.maze;

/**
 * @author gonggy
 */
public enum Direction {

	UP(0), DOWN(1), LEFT(2), RIGHT(3);

	private int index;

	private Direction(int index) {
		this.index = index;
	}

	public static Direction valueOf(int index) {
		for (Direction direction : values()) {
			if (direction.index == index) {
				return direction;
			}
		}
		return Direction.UP;
	}
}
