package com.uyong.study.maze;

/**
 * @author gonggy
 */
public final class BlockStatus {

	public static final int INIT = 0;
	public static final int OPEN = 1;
	public static final int COMMON = 2;
	public static final int VISTID = 3;

}
