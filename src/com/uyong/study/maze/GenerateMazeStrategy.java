package com.uyong.study.maze;

/**
 * 生成迷宫策略
 * 
 * @author gonggy
 */
public interface GenerateMazeStrategy {

	/**
	 * 生成迷宫数组，
	 * 
	 * @param size 迷宫大小
	 * @return
	 */
	int[][] generate(int size);
}
